package com.diploma.hospital.service;


import com.diploma.hospital.model.card.OutpatientRecord;
import com.diploma.hospital.model.card.repository.AnamnesisCardRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

@Service
@AllArgsConstructor
public class AnamnesisCardService {

    private final AnamnesisCardRepository anamnesisCardRepository;

    @Transactional
    public List<OutpatientRecord> getRecords(UUID cardId) {
        return anamnesisCardRepository.getOne(cardId).getOutpatientRecords();
    }
}
