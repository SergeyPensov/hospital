package com.diploma.hospital.controller.patient;


import com.diploma.hospital.model.card.OutpatientRecord;
import com.diploma.hospital.model.patient.repository.PatientRepository;
import com.diploma.hospital.service.AnamnesisCardService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.util.List;
import java.util.UUID;

@AllArgsConstructor
@RestController
@RequestMapping("/api/patient")
public class PatientController {

    private final AnamnesisCardService anamnesisCardService;
    private final PatientRepository patientRepository;

    @GetMapping("/card")
    public List<OutpatientRecord> getAnamnesis(Principal principal) {
        return anamnesisCardService.getRecords(patientRepository.getOne(UUID.fromString(principal.getName())).getAnamnesisCard().getAnamnesisCardId());
    }
}
