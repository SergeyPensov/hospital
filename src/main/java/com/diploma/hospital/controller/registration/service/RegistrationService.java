package com.diploma.hospital.controller.registration.service;


import com.diploma.hospital.controller.registration.data.DoctorRegistrationRequest;
import com.diploma.hospital.controller.registration.data.PatientRegistrationRequest;
import com.diploma.hospital.model.Person;
import com.diploma.hospital.model.PersonRepository;
import com.diploma.hospital.model.card.AnamnesisCard;
import com.diploma.hospital.model.card.repository.AnamnesisCardRepository;
import com.diploma.hospital.model.doctor.Doctor;
import com.diploma.hospital.model.doctor.Hospital;
import com.diploma.hospital.model.doctor.repository.CategoryRepository;
import com.diploma.hospital.model.doctor.repository.DoctorRepository;
import com.diploma.hospital.model.doctor.repository.HospitalRepository;
import com.diploma.hospital.model.patient.Patient;
import com.diploma.hospital.model.patient.document.Document;
import com.diploma.hospital.model.patient.document.repository.DocumentTypeRepository;
import com.diploma.hospital.model.patient.preferential.Preferential;
import com.diploma.hospital.model.patient.preferential.repository.PreferentialTypeRepository;
import com.diploma.hospital.model.patient.repository.PatientRepository;
import com.diploma.hospital.security.filters.SecurityConstants;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.UUID;


@AllArgsConstructor
@Service
public class RegistrationService {

    private final PersonRepository personRepository;
    private final PatientRepository patientRepository;
    private final DocumentTypeRepository documentTypeRepository;
    private final PreferentialTypeRepository preferentialTypeRepository;
    private final AnamnesisCardRepository anamnesisCardRepository;
    private final CategoryRepository categoryRepository;
    private final HospitalRepository hospitalRepository;
    private final DoctorRepository doctorRepository;


    private final EntityManager entityManager;


    @Transactional
    public void registerPatient(PatientRegistrationRequest request) {
        var person = Person.builder()
                .userName(request.getUserName())
                .hashedPassword(Person.getSHA_512_SecurePassword(request.getPassword(), SecurityConstants.PASSWORD_SALT))
                .role(Person.Role.ROLE_PATIENT)
                .build();
        person = personRepository.save(person);

        var document = Document.builder()
                .documentNumber(request.getDocument().getDocumentNumber())
                .documentType(documentTypeRepository.getOne(request.getDocument().getDocumentTypeId()))
                .build();

        var card = AnamnesisCard.builder().anamnesisCardId(UUID.randomUUID()).build();

        var preferential = Preferential.builder()
                .documentNumber(request.getPreferential().getDocumentNumber())
                .preferentialType(preferentialTypeRepository.getOne(request.getPreferential().getPreferentialTypeId()))
                .startDate(request.getPreferential().getStartDate())
                .endDate(request.getPreferential().getEndDate())
                .build();
        var patient = Patient.builder()
                .document(document)
                .firstName(request.getFirstName())
                .lastName(request.getLastName())
                .patronymic(request.getPatronymic())
                .dateOfBirth(request.getDateOfBirth())
                .homeAddress(request.getHomeAddress())
                .workAddress(request.getWorkAddress())
                .preferential(preferential)
                .build();
        patient = patientRepository.save(patient);
        entityManager.flush();
        card.setPatient(patient);
        anamnesisCardRepository.save(card);
        entityManager.flush();
        patient.setAnamnesisCard(card);
        patientRepository.save(patient);
        person.setPrincipalId(patient.getPatientId());
        personRepository.save(person);
    }

    @Transactional
    public void registerDoctor(DoctorRegistrationRequest request) {
        var person = Person.builder()
                .userName(request.getUserName())
                .hashedPassword(Person.getSHA_512_SecurePassword(request.getPassword(), SecurityConstants.PASSWORD_SALT))
                .role(Person.Role.ROLE_DOCTOR)
                .build();
        person = personRepository.save(person);

        var doctor = Doctor.builder()
                .firstName(request.getFirstName())
                .lastName(request.getLastName())
                .patronymic(request.getPatronymic())
                .category(categoryRepository.getOne(request.getCategoryId()))
                .position(request.getPosition())
                .hospital(hospitalRepository.getOne(request.getHospitalId()))
                .build();

        doctorRepository.save(doctor);
        person.setPrincipalId(doctor.getDoctorId());
        personRepository.save(person);
    }
}
