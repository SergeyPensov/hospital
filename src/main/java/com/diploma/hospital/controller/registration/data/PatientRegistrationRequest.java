package com.diploma.hospital.controller.registration.data;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;

import java.util.Date;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
@ToString
public class PatientRegistrationRequest {

    private String userName;

    private String password;


    private String firstName;

    private String lastName;

    private String patronymic;

    private String workAddress;

    private String homeAddress;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy.MM.dd")
    private Date dateOfBirth;

    private Document document;

    private Preferential preferential;

    @AllArgsConstructor
    @NoArgsConstructor
    @Getter
    @Setter
    @EqualsAndHashCode
    @ToString
    public static class Document {

        private UUID documentTypeId;

        private String documentNumber;

    }

    @AllArgsConstructor
    @NoArgsConstructor
    @Getter
    @Setter
    @EqualsAndHashCode
    @ToString
    public static class Preferential {

        private UUID preferentialTypeId;

        private String documentNumber;

        @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy.MM.dd")
        private Date startDate;

        @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy.MM.dd")
        private Date endDate;
    }

}
