package com.diploma.hospital.controller.registration.data;

import lombok.*;

import java.util.UUID;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
@ToString
public class DoctorRegistrationRequest {
    private String userName;

    private String password;


    private String firstName;

    private String lastName;

    private String patronymic;

    private String position;

    private UUID hospitalId; // 04e0d676-bd8d-4679-8d9d-5029c4651598

    private UUID categoryId; //29024840-f187-4927-b155-6956c056c633

}
