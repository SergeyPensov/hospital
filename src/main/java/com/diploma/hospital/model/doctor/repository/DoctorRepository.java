package com.diploma.hospital.model.doctor.repository;

import com.diploma.hospital.model.doctor.Doctor;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface DoctorRepository extends JpaRepository<Doctor, UUID> {
}
