package com.diploma.hospital.model.card.repository;

import com.diploma.hospital.model.card.OutpatientRecordType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface OutpatientRecordTypeRepository extends JpaRepository<OutpatientRecordType, UUID> {
}

